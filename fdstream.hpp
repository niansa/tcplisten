#pragma once
#include <string>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>




namespace tcplisten {
template<typename... Args>
inline auto libc_read(Args... args) {
    return read(args...);
}
template<typename... Args>
inline auto libc_write(Args... args) {
    return write(args...);
}

template<bool instantflush = true>
class Fdstream {
    int ifd, ofd;
    std::ostringstream in;
    std::stringstream out;

    void init() {
        fcntl(ifd, F_SETFL, O_NONBLOCK);
        if constexpr(!instantflush) {
            out_flush();
        }
    }
    void deinit() {
        if constexpr(!instantflush) {
            in_flush();
        }
    }

public:
    Fdstream(int ifd, int ofd)
        : ifd(ifd), ofd(ofd) {
        init();
    }
    Fdstream(int fd)
        : ifd(fd), ofd(fd) {
        init();
    }
    ~Fdstream() {
        deinit();
    }

    Fdstream(const Fdstream&) = delete;

    void in_flush() {
        // Write
        auto buf = in.str();
        libc_write(ofd, buf.c_str(), buf.size());
        in.str("");
    }

    void out_flush() {
        char buf[255];
        ssize_t bytes_read = sizeof(buf);
        // Read
        while (bytes_read == sizeof(buf) && !(bytes_read < 0 && errno == EAGAIN)) {
            bytes_read = libc_read(ifd, buf, sizeof(buf));
            if (bytes_read > 0) {
                out << std::string(buf, bytes_read);
            }
        }
    }

    template<typename T>
    void operator >>(T& arg) {
        if constexpr(instantflush) {
            out_flush();
        }
        out >> arg;
    }
    template<typename T>
    void operator <<(const T& arg) {
        in << arg;
        if constexpr(instantflush) {
            in_flush();
        }
    }
};
}
